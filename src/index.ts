import express from "express";
import { usersRouter } from "./usersRouter.js";
import { Request, Response } from "express";
import log from "@ajar/marker";

const { PORT = 3030, HOST = "localhost" } = process.env;

const app = express();
app.use(express.json());

app.use("/users/", usersRouter);

app.use("*", (req: Request, res: Response) => {
  res
    .status(404)
    .json(`${req.originalUrl} is unsupported route`);
});

app.listen(Number(PORT), HOST, () => {
  log.magenta(`listening on`, `http://${HOST}:${PORT}`);
});
