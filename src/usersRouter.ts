import { Request, Response, NextFunction, Router } from "express";
import morgan from "morgan";
import { User, usersObj } from "./types";
import fs from "fs/promises";

const usersRouter = Router();
const usersDBJsonPath: string = "./users.json";
const usersLogJsonPath: string = "./log";

usersRouter.use(morgan("dev"));

usersRouter.use(async (req: Request, res: Response, next: NextFunction) => {
  const users: usersObj = await getUsers();
  req.body.users = users;
  next();
});

usersRouter.use(async (req: Request, res: Response, next: NextFunction) => {
  await fs.appendFile(
    usersLogJsonPath,
    `${req.method} ${req.originalUrl} ${Math.floor(Date.now() / 1000)}`
  );
  next();
});

usersRouter.post("/create/", async (req: Request, res: Response) => {
  const { firstName, lastName, email, password, users } = req.body;
  const uniqueID: string = "_" + Math.random().toString(16).slice(2);

  let user: User = {
    firstName: String(firstName),
    lastName: String(lastName),
    email: String(email),
    password: String(password),
  };

  users[uniqueID] = user;
  await updateFile(users, usersDBJsonPath);
  res.status(200).send("User added succesfuly");
});

usersRouter.put("/update/:userID", async (req: Request, res: Response) => {
  const { userID } = req.params;
  const { firstName, lastName, email, password, users } = req.body;
  users[userID] = {
    firstName: String(firstName),
    lastName: String(lastName),
    email: String(email),
    password: String(password),
  };

  await updateFile(users, usersDBJsonPath);

  res.status(200).send(users[userID]);
});

usersRouter.delete("/delete/:userID", async (req: Request, res: Response) => {
  const { users } = req.body;
  const { userID } = req.params;

  delete users[userID];

  await updateFile(users, usersDBJsonPath);

  res.status(200).json(`user with ID : ${userID} deleted succesfuly`);
});

usersRouter.get("/read/:userID", async (req: Request, res: Response) => {
  const { users } = req.body;
  const { userID } = req.params;

  res.status(200).send(users[userID]);
});

usersRouter.get("/all", async (req: Request, res: Response) => {
  const { users } = req.body;
  res.status(200).json(users);
});

async function getUsers(): Promise<usersObj> {
  const fileExists = async (path: string) =>
    !!(await fs.stat(path).catch((e) => false));

  if (await fileExists(usersDBJsonPath)) {
    return JSON.parse(await fs.readFile(usersDBJsonPath, "utf-8"));
  } else {
    return {};
  }
}

async function updateFile(users: any, path: string) {
  await fs.writeFile(path, JSON.stringify(users, null, 2));
}

export { usersRouter };
